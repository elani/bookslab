﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksLab.DAL
{
    public class BooksLabRepository
    {
        public DataSet dataSet = new DataSet();
        private string cnString = string.Empty;
        private SqlDataAdapter BooksAdapter = null;
        private SqlDataAdapter AuthorsAdapter = null;
        public BooksLabRepository(string connectionString)
        {
            cnString = connectionString;
            ConfigureAdapters();
        }

        private void ConfigureAdapters()
        {
            string books = "Select * from dbo.Books";
            BooksAdapter = new SqlDataAdapter(books, cnString);
            SqlCommandBuilder builder = new SqlCommandBuilder(BooksAdapter);
            string authors = "Select * from dbo.Authors";
            AuthorsAdapter = new SqlDataAdapter(authors, cnString);
            SqlCommandBuilder aBuilder = new SqlCommandBuilder(AuthorsAdapter);
            AuthorsAdapter.Fill(dataSet, "Authors");
            dataSet.Tables["Authors"].Columns["Id"].AutoIncrement = true;
            //dataSet.Tables["Authors"].Columns["Id"].AutoIncrementSeed = 1;
            dataSet.Tables["Authors"].Columns["Id"].AutoIncrementStep = 1;
            BooksAdapter.Fill(dataSet, "Books");

            dataSet.Tables["Books"].Columns["Id"].AutoIncrement = true;
           // dataSet.Tables["Books"].Columns["Id"].AutoIncrementSeed = 
            dataSet.Tables["Books"].Columns["Id"].AutoIncrementStep = 1;
            BuildTableRelations();
        }
        public DataTable GetAllBooks()
        {
            return dataSet.Tables["Books"];
        }
        public DataTable GetAllAuthors()
        {
            return dataSet.Tables["Authors"];
        }
        public void Update()
        {
            AuthorsAdapter.Update(dataSet, "Authors");
            BooksAdapter.Update(dataSet, "Books");
        }
       
        private void BuildTableRelations()
        {
            DataRelation dr = new DataRelation("BookAuthor",
                    dataSet.Tables["Authors"].Columns["Id"],
                    dataSet.Tables["Books"].Columns["AuthorId"]
                );
            dataSet.Relations.Add(dr);
        }
    }
}
