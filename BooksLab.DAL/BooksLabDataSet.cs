﻿using BooksLab.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksLab.DAL
{
    public class BooksLabDataSet : DataSet
    {
        public List<Book> Books { get; set; }
        public List<Author> Authors { get; set; }
        public BooksLabDataSet()
        {
            string queryString =
                 "SELECT * FROM dbo.Books";
            using (SqlConnection cn = new SqlConnection("Data Source=LENINGRAD;Initial Catalog=BooksLab;Integrated Security=True;Pooling=False"))
            {
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, cn);
                adapter.Fill(this, "Books");


            }
        }
    }
}
