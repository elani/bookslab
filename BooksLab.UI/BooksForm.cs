﻿using BooksLab.Model;
using BooksLab.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace BooksLab.UI
{
    public partial class BooksForm : Form
    {

        private static string conn = ConfigurationManager.ConnectionStrings["BooksLabEntities"].ConnectionString;
        private BooksLabRepository repo = new BooksLabRepository(conn);
        public BooksForm()
        {
            InitializeComponent();

            dgvBooks.DataSource = repo.GetAllBooks();
            dgvAuthors.DataSource = repo.GetAllAuthors();
        }

        private void btnUpdateBooks_Click(object sender, EventArgs e)
        {
            try
            {
                repo.Update();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


       

    }
}
